var debug = process.env.LDAP_DEBUG ? console.log : function () {};

var ldapjs = Npm.require('ldapjs');

Accounts._checkPassword = function (username, password) {
    debug('[DEBUG] _checkPassword', username, password);

    try {
        var ldapClient = ldapjs.createClient({ url: process.env.CLOUDRON_LDAP_URL });

        var ldapDn = 'cn=' + username + ',' + process.env.CLOUDRON_LDAP_USERS_BASE_DN;

        var ldapBindSync = Meteor.wrapAsync(ldapClient.bind, ldapClient);
        ldapBindSync(ldapDn, password);

        debug('[DEBUG] _checkPassword: successful bind');

        var data = Meteor.wrapAsync(function ldapSearch(callback) {
            ldapClient.search(process.env.CLOUDRON_LDAP_USERS_BASE_DN, { filter: '(|(mail=' + username + ')(username=' + username + '))' }, function (error, result) {
                if (error) return callback(error);

                var items = [];

                result.on('searchEntry', function(entry) { items.push(entry.object); });
                result.on('error', callback);

                result.on('end', function (result) {
                    if (result.status !== 0) return callback(new Error('non-zero status from LDAP search'));
                    if (items.length === 0) return callback(new Error('Duplicate entries found'));

                    callback(null, items[0]);
                });
            });
        })();

        debug('[DEBUG] _checkPassword result', data);

        return {
            cloudronId: data.uid,
            username: data.username,
            email: data.mail,
            displayName: data.displayname,
            admin: !!data.isadmin
        };
    } catch (e) {
        debug('[DEBUG] _checkPassword error', e);
        return { userId: null, error: new Meteor.Error(403, 'Incorrect password') };
    }
};

Accounts._findUserByQuery = function (query) {
    debug('[DEBUG] _findUserByQuery', query);

    var user = null;

    if (query.id) {
        user = Meteor.users.findOne({ _id: query.id.toLowerCase() });
    } else {
        var fieldName;
        var fieldValue;
        if (query.username) {
            fieldName = 'username';
            fieldValue = query.username;
        } else if (query.email) {
            fieldName = 'emails.address';
            fieldValue = query.email;
        } else {
            throw new Error('shouldn\'t happen (validation missed something)');
        }
        var selector = {};
        selector[fieldName] = fieldValue.toLowerCase();
        user = Meteor.users.findOne(selector);
    }

    debug('[DEBUG] _findUserByQuery found user', user);
    return user;
};

Accounts.registerLoginHandler('password', function (options) {
    debug('[DEBUG] password login handler', options);

    var profile = Accounts._checkPassword(options.user.username || options.user.email, options.password);

    if (profile.error) {
        debug('[DEBUG] invalid password');
        return profile;
    }

    var user = Accounts._findUserByQuery(options.user);
    if (user) {
        debug('[DEBUG] user found all good', user, profile);

        debug('[DEBUG] now update user record');

        Accounts.updateProfile(user._id, profile);

        return {
            userId: user._id
        };
    }

    debug('[DEBUG] no user found create user on the fly');
    var userId = Accounts.createUser({
        cloudronId: profile.cloudronId,
        username: profile.username,
        profile: {
            fullname: profile.displayName
        },
        emails: [{ address: profile.email, verified: true }],
        admin: profile.admin
    });

    return {
        userId: userId
    };
});

Accounts.updateProfile = function (userId, profile) {
    debug('[DEBUG] updateProfile', userId, profile);

    var user = Meteor.users.findOne(userId);
    if (!user) throw new Meteor.Error(403, 'User not found');

    Meteor.users.update({ _id: user._id }, { $set: {
        'username': profile.username,
        'profile.fullname': profile.displayName,
        'emails': [{ address: profile.email, verified: true }]
    }});
};

Accounts.setUsername = function (userId, newUsername) {
    debug('[DEBUG] setUsername', userId, newUsername);

    var user = Meteor.users.findOne(userId);
    if (!user) throw new Meteor.Error(403, 'User not found');

    Meteor.users.update({ _id: user._id }, { $set: { username: newUsername }});
};

Accounts.setPassword = function (userId, newPlaintextPassword, options) {
    debug('[DEBUG] setPassword', userId, newPlaintextPassword, options);
};

Accounts.sendResetPasswordEmail = function (userId, email) {
    debug('[DEBUG] sendResetPasswordEmail', userId, email);
};

Accounts.sendEnrollmentEmail = function (userId, email) {
    debug('[DEBUG] sendEnrollmentEmail', userId, email);
};

Accounts.sendVerificationEmail = function (userId, address) {
    debug('[DEBUG] sendVerificationEmail', userId, address);
};

Accounts.addEmail = function (userId, newEmail, verified) {
    debug('[DEBUG] addEmail', userId, newEmail, verified);

    var user = Meteor.users.findOne(userId);
    if (!user) throw new Meteor.Error(403, 'User not found');

    Meteor.users.update({ _id: user._id }, { $set: { emails: [{ address: newEmail, verified: true }] }});
};

Accounts.removeEmail = function (userId, email) {
    debug('[DEBUG] removeEmail', userId, email);
};

Accounts.createUser = function (options) {
    debug('[DEBUG] createUser', options);

    var username = options.username;
    var emails = options.emails;
    if (!username && !emails) throw new Meteor.Error(400, 'Need to set a username or emails');

    var user = {
        admin: !!options.admin,
        services: {}
    };

    if (options.id) user._id = options.id;
    if (username) user.username = username;
    if (emails) user.emails = emails;

    var userId = Accounts.insertUserDoc(options, user);

    debug('[DEBUG] createUser resulting id', userId);

    return userId;
};
