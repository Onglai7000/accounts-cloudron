This is a meteor package as a drop-in replacement for [accounts-password](https://atmospherejs.com/meteor/accounts-password)

It can be used by placing it in the meteor app's `packages/` folder.
